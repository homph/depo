
#KTÜ CENG ÖĞRENCİ PLATFORMU#


Problem ve Motivasyon tanımı:
KTÜ'de okuyan bilgisayar mühendisiliği öğrencilerinin hem okudukları bölüm hakkında hem de şehirdeki sosyal imkanlardan haberdar olmakta güçlük çekmesi.
Bölümümüze yeni gelen öğrencilerin şehir ve bölüm hakkında yeterince bilgiye sahip olmamaları ayrıca bu konular hakkında bilgi alabilecek kimseyi 
tanımamaları.


Hedef:
En güzel yemek nerede yenir?,Nereye,nereden,ne ile gidilir?,Konaklama imknaları nasıl?,Etkinliklerden nasıl haberim olur?,İkinci el kitaplarımı nasıl
satabilirim-nereden alabilirim?,Ders notlarına nasıl ulaşabilirim? gibi sorulara cevap veren ve hepsini tek bir platformda toplayan bir web sitesi 
yapmayı hedefliyoruz.


Fonksiyonel Gereksinimler:
Web sayfası öğrenci ve yönetici olmak üzere iki girişten oluşacak.Yönetici ve öğrencinin çeşitli sayfalarda yetkilendirme farkları olacak.Belirlenen 
çeşitli başlıklar da kullanıcının yorum yapmasına olanak sağlanacak.Yapılan yorum diğer kullanıcıların puanlamasına tabi tutulacak.Sayfaların başlıkları
okul,sosyal,ilan ve profil olarak gruplanacak.Okul başlığının altında not paylaşımı imkanı sağlanacak;paylaşılan notlar yöneticinin filtresinden 
geçirildikten sonra paylaşılabilecek.Sosyal başlığının altında çeşitli konu başlıklarında kullanıcılar fikirlerini diğer kullanıcılarla paylaşabilecekler
böylelikle öğrencinin çevreyi tanıması sağlanacak.Profil başlığının altında kullanıcı kendi bilgilerini güncelleyebilecek.İlan sayfasının altında yapmayı
hedeflediğimiz iş;kullanıcıların elden çıkarmak istedikleri eşyaları paylaşıp satabilecekleri bir platform oluşturmak.Kullanıcıların paylaşım yapmalarını
arttırmak ve aralarında bir rekabet ortamı oluşturmak için haftalık olarak en çok etkileşimde bulunanların profilleri anasayfada yayınlanacak.


Fonksiyonel Olmayan Gereksinimler:
Web sayfasına ktü bilgisayar mühendisliği veri tabanında kayıt olan öğrenciler girebilecekler böylelikle sistemin güvenilirliği sağlanmış olacak.Veri 
girişi klavye ve mouse kullanılarak yapılabilecek.Oluşturacağımız web sayfası çeşitli işletim sistemleri tarafından desteklenebilir olacak.
Demo olarak 100 kişilik bir veri tabanı kullanacağız.Aslında hedeflenen ktü bilgisayar mühendisliği veri tabanını kullanmak.

