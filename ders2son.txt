<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="s�n�f2.aspx.cs" Inherits="s�n�f2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        body{background-color:#ccf1ea}
        .metin2{top:8% ;left:30% ;position:absolute;font-size:24px;color:#014e70}
        .metin{top:24% ;left:1% ;position:absolute;color:#000000;font-size:8px;font:Times New Roman;border-style:outset;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="metin2">
        <tr>
            <td>
                <h1>
                    2.SINIF DERSLER�
            </h1>
            </td>
        </tr>
    </table>
    <table class="metin">
        <tr>
         <td>


<h1>1)Veri Yap�lar�</h1>
<h1>Dersin kredisi 3.Dersi veren e�itmen ��r.Gr.�mer �ak�r</h1>
<h1>Anabilim dal� Bilgisayar Yaz�l�m�</h1>
<h1> <a href="http://aves.ktu.edu.tr/cakiro">Kisisel Sayfas� </a></h1>
<h1>��km�� s�nav sorular�n� ��zmek faydal� olur.</h1>

<h1>2)Nesne Y�nelimli Programlama</h1>
<h1>Dersin kredisi 3,5.Dersi veren e�itmen Yrd.Do�.Dr.Sedat G�rm��</h1>
<h1>Anabilim dal� Bilgisayar Yaz�l�m�</h1>
<h1> <a href="http://aves.ktu.edu.tr/sedatgormus">Kisisel Sayfas� </a></h1>
<h1>Bu derste c++ programlama dili anlat�l�yor.Kodlar� yazmak ve derlemek i�in code blocks ya da dev c++ ideleri kullan�l�yor.</h1>
<h1>Ders teorik ve uygulamal� olarak i�leniyor.</h1>
<h1>Dersi iyi �grenmek i�in ve iyi yaz�l�mc� olmak i�in ve ge�mek i�in bol bol �rnek kodlar yaz�lmal�d�r.</h1>

<h1>3)Say�sal Tasar�m</h1>
<h1>Dersin kredisi 4.Dersi veren e�itmen Prof.Dr. Murat Ekinci</h1>
<h1>Anabilim dal� Bilgisayar Bilimleri</h1>
<h1> <a href="http://aves.ktu.edu.tr/ekinci">Kisisel Sayfas� </a></h1>
<h1>Derste rom,flip flop t�rleri,pla,asenkron ard���l devreler tasar�mlar�,</h1>
<h1>ilkel ak�� tablosu,riskler,yar�� durumlar� gibi konular i�lenmektedir.</h1>
<h1>Eski s�nav sorular�n� ��zerseniz iyi olur. </h1>

<h1>4)Ayr�k Matematik</h1>
<h1>Dersin kredisi 3.Dersi veren e�itmen Prof.Dr. Prof.Dr. Vas�f Nabiyev</h1>
<h1> <a href="http://aves.ktu.edu.tr/vasif">Kisisel Sayfas� </a></h1>
<h1>Derste boole fonksiyonlar�n�n minimizasyonu,boole fonksiyonlar�n�n s�n�fland�r�lmas�,</h1>
<h1>graf teorisi,a��l�m a�a�lar� algotitmalar�,hasse diyagram� gibi konular anlat�lmaktad�r.</h1>
<h1>Prof.Dr. Vas�f Nabiyev'in Algoritmalar kitab� bu derste ve algotitmalar dersinde �ok�a</h1>
<h1>i�inize yarar.Ayr�ca bilgi da�arc���n�z� geli�tirmenize fayda sa�lar.</h1>

<h1>5)Diferansiyel Denklemler</h1>
<h1>Dersin kredisi 4.Dersi matematik b�l�m�nden gelen e�itmen veriyor.</h1>
<h1>Derste tam diferensiyel denklemler,homojen denklemler,laplace denklemler gibi konular anlat�lmaktad�r.</h1>

<h1>6)Elektronik Laboratuvar</h1>
<h1>Dersin kredisi 1,5.Elektronik laboratuvar�nda uygulamal� olarak yap�l�yor.</h1>
<h1>9 tane deneyi olu�turulan 3 er ki�ilik grup arkada�lar�n ile birlikte ara�t�ma g�revlilerinin </h1>
<h1>yard�m� ile devreleri ger�ekle�tiriyoruz.Deneylere haz�rl�kl� gelinmeli soru soruluyor.</h1>
<h1>Deney sonralar� da rapor olu�turuluyor.Bu raporlar�n de�erlendirilmesi vize notu yerine ge�iyor.</h1>
<h1>Finalleri ise uygulamal� oluyor.</h1>

<h1>7)Programlama Dilleri</h1>
<h1>Dersin kredisi 3.Dersi veren e�itmen Yrd.Do�.Dr. H�seyin Pehlivan.</h1>
<h1>Anabilim dal� Bilgisayar Yaz�l�m�</h1>
<h1> <a href="http://aves.ktu.edu.tr/pehlivan">Kisisel Sayfas� </a></h1>
<h1>Derste pascal programlama dili kullan�m� anlat�l�yor.</h1>
<h1>��lenen slaytlara dersin sayfas�ndan ula�abilirsiniz.</h1>

<h1>8)Otomato Teorisi</h1>
<h1>Dersin kredisi 3.Dersi veren e�itmen ��r.Gr.�mer �ak�r</h1>
<h1> <a href="http://aves.ktu.edu.tr/cakiro">Kisisel Sayfas� </a></h1>
<h1>Derste i�lenen pdf dersin sayfas�nda yay�mlan�yor.</h1>
<h1>��km�� s�nav sorular�n� incelerseniz s�navda ba�ar� elde edebilirsiniz.</h1>

<h1>9)Mikroi�lemciler</h1>
<h1>Dersin kredisi 4.Dersi veren e�itmen Do�.Dr. Mustafa Uluta�.</h1>
<h1> <a href="http://aves.ktu.edu.tr/ulutas">Kisisel Sayfas� </a></h1>
<h1>Mikroi�lemciler dersi mikroi�lemciler laboratuvar dersini kilitliyor.</h1>

<h1>10)Algoritmalar</h1>
<h1>Dersin kredisi 3.Dersi veren e�itmen Prof.Dr. Prof.Dr. Vas�f Nabiyev</h1>
<h1> <a href="http://aves.ktu.edu.tr/vasif">Kisisel Sayfas� </a></h1>
<h1>Derste turing machine,�arpma algotitmalar�,say� sistemleri,veri g�venli�i algotitmalar�,</h1>
<h1>gibi konular i�lenmektedir.</h1>
<h1>Prof.Dr. Vas�f Nabiyev'in Algoritmalar kitab� bu derste ve algotitmalar dersinde �ok�a</h1>
<h1>i�inize yarar.Ayr�ca bilgi da�arc���n�z� geli�tirmenize fayda sa�lar.</h1>

<h1>11)M�hendislik Matemati�i</h1>
<h1>Dersin kredisi 3.Dersi veren e�itmen Prof.Dr. Murat Ekinci</h1>
<h1>Anabilim dal� Bilgisayar Bilimleri</h1>
<h1> <a href="http://aves.ktu.edu.tr/ekinci">Kisisel Sayfas� </a></h1>
<h1>��lenen konular fourier serisi,ayr�k fourier serisi,ters fourier serisi,</h1>
<h1>gauss jordon,matris transformasyonu,gibi konular i�lenmektedir.</h1>

<h1>12)Say�sal Tasar�m Laboratuvar�</h1>
<h1>Dersin kredisi 1,5.Elektronik laboratuvar�nda uygulamal� olarak yap�l�yor.</h1>
<h1>9 tane deneyi olu�turulan 3 er ki�ilik grup arkada�lar�n ile birlikte ara�t�ma g�revlilerinin </h1>
<h1>yard�m� ile devreleri ger�ekle�tiriyoruz.Deneylere haz�rl�kl� gelinmeli soru soruluyor.</h1>
<h1>Deney sonralar� da rapor olu�turuluyor.Bu raporlar�n de�erlendirilmesi vize notu yerine ge�iyor.</h1>

              </td>
       </tr>
    </table>

</asp:Content>

